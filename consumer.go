package consumers

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type Consumer interface {
	Start()
	Close()
	Consume(message *kafka.Message)
	Started() bool
}

type MessageReader interface {
	Read(message *kafka.Message)
}

type goTestConsumer struct {
	reader   MessageReader
	consumer *kafka.Consumer
	sigchan  chan os.Signal
	running  bool
	started  bool
}

func (consumer *goTestConsumer) Start() {
	consumer.running = true

	for consumer.running == true {
		select {
		case _ = <-consumer.sigchan:
			consumer.Close()
		case ev := <-consumer.events():
			switch e := ev.(type) {
			case kafka.AssignedPartitions:
				consumer.assign(e.Partitions)
			case kafka.RevokedPartitions:
				consumer.unassign()
			case *kafka.Message:
				consumer.Consume(e)
			case kafka.Error:
				consumer.Close()
			}
		}
	}
}

func (consumer *goTestConsumer) Started() bool {
	return consumer.started
}

func (consumer *goTestConsumer) events() chan kafka.Event {
	return consumer.consumer.Events()
}

func (consumer *goTestConsumer) assign(partitions []kafka.TopicPartition) {
	consumer.consumer.Assign(partitions)
	consumer.started = true
}

func (consumer *goTestConsumer) unassign() {
	consumer.consumer.Unassign()
}

func (consumer *goTestConsumer) Close() {
	if !consumer.running {
		return
	}

	consumer.running = false
	consumer.consumer.Close()
	close(consumer.sigchan)
}

func (consumer *goTestConsumer) Consume(message *kafka.Message) {
	consumer.reader.Read(message)
}

func NewConsumer(reader MessageReader) (Consumer, error) {
	broker := "localhost:9092"
	topics := []string{"go-consumer-topic"}
	group := "go-test"

	sigchan := make(chan os.Signal)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":               broker,
		"group.id":                        group,
		"session.timeout.ms":              6000,
		"go.events.channel.enable":        true,
		"go.application.rebalance.enable": true,
		"default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "earliest"},
	})

	if err != nil {
		return nil, err
	}

	err = c.SubscribeTopics(topics, nil)

	if err != nil {
		return nil, err
	}

	return &goTestConsumer{reader, c, sigchan, false, false}, nil
}
