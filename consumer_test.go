package consumers

import (
	"fmt"
	"testing"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type fakeMessageReader struct {
	messages []*kafka.Message
}

func (reader *fakeMessageReader) Read(message *kafka.Message) {
	messages := append(reader.messages, message)

	reader.messages = messages
}

func TestConsumingMessage(t *testing.T) {
	reader := fakeMessageReader{}
	consumer, err := NewConsumer(&reader)

	if err != nil {
		panic(err)
	}

	go func() {
		consumer.Start()
	}()

	for !consumer.Started() {
		time.Sleep(1)
	}

	produceExampleMessage()

	if len(reader.messages) < 1 {
		t.Error("Expected at least one messages to be processed")
	}

	message := reader.messages[0]

	if string(message.Value) != "Hello Go" {
		t.Error("Expecting 'Hello Go' message to be received")
	}
}

func produceExampleMessage() {
	broker := "localhost:9092"
	topic := "go-consumer-topic"

	producer, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": broker, "group.id": "go-test"})

	if err != nil {
		panic(err)
	}

	deliveryChan := make(chan kafka.Event)

	value := "Hello Go"
	err = producer.Produce(&kafka.Message{TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny}, Value: []byte(value)}, deliveryChan)

	if err != nil {
		panic(err)
	}

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
	}

	close(deliveryChan)
}
